# 과적합 방지와 정규화 <sup>[1](#footnote_1)</sup>

> <font size="3">과적합을 방지하는 방법을 배우고 기계 학습 모델에 정규화 기법을 적용한다.</font>

## 목차

1. [개요](./overfitting-prevention-and-regularization.md#intro)
1. [과적합이란, 왜 문제가 되는가?](./overfitting-prevention-and-regularization.md#sec_02)
1. [과적합 감지와 측정 방법](./overfitting-prevention-and-regularization.md#sec_03)
1. [데이터 조작으로 과적합 방지](./overfitting-prevention-and-regularization.md#sec_04)
1. [모델 선택과 검증을 통한 과적합 방지](./overfitting-prevention-and-regularization.md#sec_05)
1. [정규화 기법으로 과적합 방지](./overfitting-prevention-and-regularization.md#sec_06)
1. [Python에서 정규화 기법 적용](./overfitting-prevention-and-regularization.md#sec_027)
1. [마치며](./overfitting-prevention-and-regularization.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 21 — Overfitting Prevention and Regularization](https://levelup.gitconnected.com/ml-tutorial-21-overfitting-prevention-and-regularization-85b08b561d32?sk=ec5312e054ddd551f6da98c4207590ae)를 편역하였습니다.
